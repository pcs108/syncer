black==23.9.0
click==8.1.7
flake8==6.1.0
pytest==7.4.2
pytest-env==1.0.1
pytest-mock==3.11.1
