#!/usr/bin/env python3
import dataclasses
import os
import subprocess
import sys
import typing
from argparse import ArgumentParser
from pathlib import Path

USERNAME = os.getlogin()
DEFAULT_MEDIA_DEVICES_ROOT = f"/media/{USERNAME}"
MEDIA_DEVICES_ROOT = Path(
    os.getenv("SYNCER_MEDIA_DEVICES_ROOT") or DEFAULT_MEDIA_DEVICES_ROOT
).absolute()
DEFAULT_IGNORE_LIST = [".ignore"]


@dataclasses.dataclass
class Local:
    path: Path

    @classmethod
    def for_user_input(cls, user_input: str) -> "Local":
        if user_input.startswith("/"):
            path = Path(user_input)
        else:
            path = (Path.home() / user_input).absolute()
        if not path.exists():
            raise FileNotFoundError(f"Cannot sync local directory: {user_input}")
        return cls(path=path)


@dataclasses.dataclass
class Device:
    UNMOUNT_COMMAND = "umount {path!r}"

    path: Path

    @classmethod
    def for_device_info(cls, device_info: typing.Optional[str] = None) -> "Device":
        path = Device.get_device_path(device_info)
        if not path.exists():
            raise FileNotFoundError(f"Cannot sync to device path: {str(path)}")
        return cls(path=path)

    @staticmethod
    def get_device_path(device_info: typing.Optional[str] = None) -> Path:
        if device_info and device_info.startswith("/"):
            return Path(device_info).absolute()

        MEDIA_DEVICES_ROOT = Path(
            os.getenv("SYNCER_MEDIA_DEVICES_ROOT") or DEFAULT_MEDIA_DEVICES_ROOT
        ).absolute()
        if device_info:
            return (MEDIA_DEVICES_ROOT / device_info).absolute()

        configured_external_media_root = os.getenv("SYNCER_EXTERNAL_MEDIA_ROOT")
        if configured_external_media_root:
            return Path(configured_external_media_root).absolute()

        devices = [f for f in MEDIA_DEVICES_ROOT.iterdir() if f.is_dir()]

        try:
            device, *others = devices
        except ValueError:
            raise FileNotFoundError(
                f"No media devices found: {str(MEDIA_DEVICES_ROOT)}"
            )
        if others:
            try:
                device, *others = [
                    d for d in devices if "sportgo" in d.name.lower().replace(" ", "")
                ]
            except ValueError:
                raise ValueError(f"Multiple devices found: {str(MEDIA_DEVICES_ROOT)}")
            if others:
                raise ValueError(f"Multiple devices found: {str(MEDIA_DEVICES_ROOT)}")
        return device.absolute()

    def unmount(self) -> None:
        command = Device.UNMOUNT_COMMAND.format(path=str(self.path))
        subprocess.run(command, shell=True, check=True)


@dataclasses.dataclass
class External:
    path: Path
    device: Device

    @classmethod
    def for_local_path_and_device(cls, local: Local, device: Device) -> "External":
        path = device.path / local.path.name
        if not path.exists():
            raise FileNotFoundError(f"Cannot sync to external path: {str(path)}")
        return cls(path=path, device=device)


@dataclasses.dataclass
class Syncer:
    RSYNC_COMMAND = (
        "rsync -rtDvz --ignore-existing --progress --delete "
        "{filter} {exclude} {local!r} {external!r}"
    )

    RSYNC_FILTER = "--filter 'protect *.POS'"

    local: Local
    external: External
    ignore: typing.List[str] = dataclasses.field(default_factory=lambda: [])

    @property
    def command(self) -> str:
        return Syncer.RSYNC_COMMAND.format(
            filter=Syncer.RSYNC_FILTER,
            exclude=self.exclude_args,
            local=self.local_arg,
            external=self.external_arg,
        )

    @property
    def exclude_args(self) -> str:
        ignored_patterns = [f"*/{i}" for i in self.ignore]
        return " ".join([f"--exclude {pattern!r}" for pattern in ignored_patterns])

    @property
    def external_arg(self) -> str:
        return str(self.external.path) + "/"

    @property
    def local_arg(self) -> str:
        return str(self.local.path) + "/"

    def execute(self) -> None:
        subprocess.run(self.command, shell=True, check=True)


@dataclasses.dataclass
class POSCleaner:
    external: External

    def execute(self) -> None:
        for listing in os.walk(self.external.path):
            orphans = POSCleaner.get_orphaned_pos_files_for_directory_listing(*listing)
            for orphan in orphans:
                print(f"Cleaning up: {orphan.name}")
                orphan.unlink()

    @staticmethod
    def get_orphaned_pos_files_for_directory_listing(
        directory: str, _, file_names: typing.List[str]
    ) -> typing.List[Path]:
        audio_file_names_without_ext = POSCleaner.get_audio_file_names_without_ext(
            file_names
        )
        pos_file_names = POSCleaner.get_pos_file_names(file_names)
        return [
            Path(os.path.join(directory, f))
            for f in POSCleaner.get_pos_file_names_without_matching_audio_file(
                audio_file_names_without_ext, pos_file_names
            )
        ]

    @staticmethod
    def get_audio_file_names_without_ext(
        file_names: typing.List[str],
    ) -> typing.List[str]:
        return [os.path.splitext(f)[0] for f in file_names if f.endswith(".mp3")]

    @staticmethod
    def get_pos_file_names(file_names: typing.List[str]) -> typing.List[str]:
        return [f for f in file_names if f.endswith(".POS")]

    @staticmethod
    def get_pos_file_names_without_matching_audio_file(
        audio_file_names_without_ext: typing.List[str], pos_file_names: typing.List[str]
    ) -> typing.List[str]:
        return [
            f
            for f in pos_file_names
            if os.path.splitext(f)[0] not in audio_file_names_without_ext
        ]


def parse_args(args: typing.List[str]) -> dict:
    parser = ArgumentParser(
        prog="syncer", description="Syncs local directory to external device."
    )
    parser.add_argument("local", help="local path to be synced")
    parser.add_argument(
        "device",
        help="[optional] root path of device to be synced to",
        nargs="?",
        default="",
    )
    parser.add_argument(
        "-c", "--clean", help="clean POS files after syncing", action="store_true"
    )
    parser.add_argument(
        "-u", "--unmount", help="unmount device after syncing", action="store_true"
    )
    parsed = parser.parse_args(args)

    local = Local.for_user_input(parsed.local)
    device = Device.for_device_info(getattr(parsed, "device", None))
    external = External.for_local_path_and_device(local=local, device=device)

    clean = parsed.clean
    unmount = parsed.unmount

    return {"local": local, "external": external, "clean": clean, "unmount": unmount}


def main(
    local: Local,
    external: External,
    clean: bool,
    unmount: bool,
    ignore: typing.List[str] = DEFAULT_IGNORE_LIST,
) -> None:
    syncer = Syncer(local=local, external=external, ignore=ignore)
    syncer.execute()

    if clean:
        cleaner = POSCleaner(external)
        cleaner.execute()
    if unmount:
        external.device.unmount()


if __name__ == "__main__":
    parsed = parse_args(sys.argv[1:])
    main(**parsed)
