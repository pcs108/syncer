# syncer

Sync local directory to external media device.

```bash
$ ./syncer.py /home/user/LocalFiles
```

This automatically finds the mounted external media device path and rsyncs with that directory on the device:

```bash
rsync ... '/home/user/LocalFiles' '/media/user/foo/LocalFiles'
```
