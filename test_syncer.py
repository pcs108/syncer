import os
import shutil
from pathlib import Path

import pytest

from syncer import Device, External, Local, POSCleaner, Syncer, main, parse_args

PROJECT_ROOT = Path(__file__).parent
TEST_FILES = PROJECT_ROOT / ".test"


@pytest.fixture(autouse=True)
def handle_test_files():
    def _cleanup():
        if TEST_FILES.exists():
            shutil.rmtree(TEST_FILES)

    _cleanup()
    TEST_FILES.mkdir(exist_ok=True)
    yield
    _cleanup()


@pytest.fixture
def local_path():
    path = (TEST_FILES / "local").absolute()
    path.mkdir()
    return path


@pytest.fixture
def local(local_path):
    return Local.for_user_input(str(local_path))


@pytest.fixture
def device_path():
    path = (TEST_FILES / "media" / "device").absolute()
    path.mkdir(parents=True)
    return path


@pytest.fixture
def device(device_path):
    return Device.for_device_info(str(device_path))


@pytest.fixture
def external_path(device_path, local_path):
    path = device_path / local_path.name
    path.mkdir()
    return path


@pytest.fixture
def external(local, device, external_path):
    return External.for_local_path_and_device(local=local, device=device)


@pytest.fixture
def mocked_subprocess_run(mocker):
    return mocker.patch("syncer.subprocess.run")


@pytest.fixture
def syncer(local_path, external_path, device_path):
    return Syncer(
        local=Local(local_path),
        external=External(path=external_path, device=device_path),
    )


def test_local_for_user_input_accepts_a_full_path(local_path):
    assert Local.for_user_input(str(local_path)) == Local(local_path)


def test_local_for_user_input_defaults_to_relative_path_in_home_directory(mocker):
    mocker.patch("syncer.Path.home", return_value=TEST_FILES)
    path = TEST_FILES / "hello"
    path.mkdir()
    assert Local.for_user_input("hello") == Local(path)


def test_local_raises_error_if_path_does_not_exist():
    with pytest.raises(FileNotFoundError):
        Local.for_user_input("/hello/world")


def test_device_for_device_info_defaults_to_env_variable_if_set(mocker, device_path):
    mocker.patch.dict(os.environ, {"SYNCER_EXTERNAL_MEDIA_ROOT": str(device_path)})
    assert Device.for_device_info() == Device(device_path)


def test_device_for_device_info_constructs_from_full_path_if_passed_in(device_path):
    assert Device.for_device_info(str(device_path)) == Device(device_path)


def test_device_for_device_info_constructs_from_device_name_if_passed_in(device_path):
    assert Device.for_device_info("device") == Device(device_path)


def test_device_for_device_info_selects_single_entry_if_directory(device_path):
    assert Device.for_device_info() == Device(device_path)


def test_device_for_device_info_selects_single_directory_in_user_media_devices(
    device_path,
):
    (TEST_FILES / "foo").write_text("")
    assert Device.for_device_info() == Device(device_path)


@pytest.mark.parametrize("device_info", ["foo", "/media/foo"])
def test_device_for_device_info_raises_error_if_device_does_not_exist(device_info):
    with pytest.raises(FileNotFoundError):
        Device.for_device_info(device_info)


def test_device_for_device_info_selects_sportgo_name_from_multiple_directories(
    device_path,
):
    sportgo_path = device_path.parent / "example SpOrT go DEVICE"
    sportgo_path.mkdir()
    assert Device.for_device_info() == Device(sportgo_path)


def test_device_for_device_info_does_not_select_from_multiple_directories(device_path):
    (device_path.parent / "other").mkdir()
    with pytest.raises(ValueError):
        Device.for_device_info()


def test_device_for_device_info_does_not_select_if_no_directories():
    with pytest.raises(FileNotFoundError):
        Device.for_device_info()


def test_device_unmount_calls_command(mocked_subprocess_run, device_path):
    device = Device(device_path)
    device.unmount()

    mocked_subprocess_run.assert_called_with(
        Device.UNMOUNT_COMMAND.format(path=str(device_path)), shell=True, check=True
    )


def test_external_for_local_path_and_device(local, device, external_path):
    assert External.for_local_path_and_device(local=local, device=device) == External(
        path=external_path, device=device
    )


def test_external_for_local_path_and_device_raises_error_if_does_not_exist(
    local, device
):
    with pytest.raises(FileNotFoundError):
        External.for_local_path_and_device(local=local, device=device)


def test_syncer_exclude_args_defaults_to_empty_string(syncer):
    assert syncer.exclude_args == ""


def test_syncer_exclude_args_ignores_single_specified_pattern(syncer):
    syncer.ignore = ["._ignore"]
    assert syncer.exclude_args == "--exclude '*/._ignore'"


def test_syncer_exclude_args_ignores_multiple_specified_patterns(syncer):
    syncer.ignore = ["._ignore", "._other"]
    assert syncer.exclude_args == "--exclude '*/._ignore' --exclude '*/._other'"


def test_syncer_local_arg_adds_training_slash(local_path, syncer):
    assert syncer.local_arg == str(local_path) + "/"


def test_syncer_external_arg_adds_training_slash(external_path, syncer):
    assert syncer.external_arg == str(external_path) + "/"


def test_syncer_command_constructs_rsync_command_from_attributes(
    local_path, external_path, syncer
):
    syncer.ignore = ["all"]
    assert syncer.command == (
        "rsync -rtDvz --ignore-existing --progress --delete "
        "--filter 'protect *.POS' --exclude '*/all' "
        f"'{str(local_path)}/' '{str(external_path)}/'"
    )


def test_syncer_execute_runs_shell_command(mocked_subprocess_run, syncer):
    syncer.execute()
    mocked_subprocess_run.assert_called_with(syncer.command, shell=True, check=True)


def test_pos_cleaner_execute_removes_orphaned_pos_files(external_path, external):
    a1 = external_path / "a.mp3"
    a1.write_text("")
    a2 = external_path / "b.mp3"
    a2.write_text("")
    a3 = external_path / "c.mp3"
    a3.write_text("")

    p1 = external_path / "a.POS"
    p1.write_text("")
    p3 = external_path / "c.POS"
    p3.write_text("")

    # POS file with no matching audio file gets removed
    (external_path / "z.POS").write_text("")

    cleaner = POSCleaner(external)
    cleaner.execute()
    assert sorted(list(external_path.iterdir())) == [p1, a1, a2, p3, a3]


def test_parse_args_with_no_device_provided(local_path, device_path, external_path):
    assert parse_args([str(local_path)]) == {
        "local": Local(local_path),
        "external": External(path=external_path, device=Device(device_path)),
        "clean": False,
        "unmount": False,
    }


def test_parse_args_with_device_provided(local_path, device_path, external_path):
    assert parse_args([str(local_path), "device"]) == {
        "local": Local(local_path),
        "external": External(path=external_path, device=Device(device_path)),
        "clean": False,
        "unmount": False,
    }


def test_parse_args_with_unmount(local_path, device_path, external_path):
    assert parse_args([str(local_path), "-u"]) == {
        "local": Local(local_path),
        "external": External(path=external_path, device=Device(device_path)),
        "clean": False,
        "unmount": True,
    }


def test_parse_args_with_clean(local_path, device_path, external_path):
    assert parse_args([str(local_path), "-c"]) == {
        "local": Local(local_path),
        "external": External(path=external_path, device=Device(device_path)),
        "clean": True,
        "unmount": False,
    }


def test_main_creates_syncer_with_args_and_executes(
    mocked_subprocess_run, local_path, local, external_path, external
):
    main(
        local=local,
        external=external,
        clean=False,
        unmount=False,
    )
    mocked_subprocess_run.assert_called_with(
        Syncer.RSYNC_COMMAND.format(
            filter=Syncer.RSYNC_FILTER,
            exclude="--exclude '*/.ignore'",
            local=str(local_path) + "/",
            external=str(external_path) + "/",
        ),
        shell=True,
        check=True,
    )


def test_main_unmounts_device_if_specified(
    mocked_subprocess_run, local, external, device_path
):
    main(
        local=local,
        external=external,
        clean=False,
        unmount=True,
    )
    mocked_subprocess_run.assert_called_with(
        Device.UNMOUNT_COMMAND.format(path=str(device_path)),
        shell=True,
        check=True,
    )


def test_main_cleans_pos_files_if_specified(
    mocked_subprocess_run, local, external, external_path
):
    a1 = external_path / "a.mp3"
    a1.write_text("")

    p1 = external_path / "a.POS"
    p1.write_text("")
    p2 = external_path / "b.POS"
    p2.write_text("")

    main(
        local=local,
        external=external,
        clean=True,
        unmount=False,
    )

    assert sorted(list(external_path.iterdir())) == [p1, a1]
